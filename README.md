# Slick BeepBox Theme

One-file custom CSS theme by [Tim Krief](http://links.timkrief.com) to be applied to [John Nesky](https://johnnesky.com/)'s amazing tool, [beepbox.co](https://beepbox.co).

Check the introductory video [here](https://youtu.be/LAf2w3HJVI0)!

![Slick BeepBox Theme Screenshot](./screenshot.png "Slick BeepBox")

## Instructions
As explained in [the introductory video](https://youtu.be/LAf2w3HJVI0):
- In [BeepBox](https://beepbox.co), enable the following preference:
    - Full-Screen Layout
- These preferences are optional but recommended:
    - Show Piano Keys
    - Show All Channels
    - Octave Scroll Bar
    - Enable Channel Muting
- Add the content of [style.css](./style.css) as a custom style for [beepbox.co](https://beepbox.co)'s domain using a browser add-on like [Stylus](https://github.com/openstyles/stylus).
- Enjoy!

## Notes
The filters applied to notes to make them look smooth only work in Firefox based browsers. The issue is not the filters themeselves, it's passing them as URLs to make the theme work as a simple client-side custom style.

I'm using an MIT license so that this theme is a free/libre and opensource software, compatible with BeepBox's license.

That way, you can:
- Add this theme natively to your modded versions of BeepBox just by crediting me by including the license.
- Fork and customize the theme to your liking

Either way, [let me know](http://links.timkrief.com) :smile:. I can't wait to see how you're going to use my theme!

## Customization
The theme has been made with customization in mind. You can easily change the values of CSS variables that are at the begining of the file. You can also, if you want to, remove entire sections of the file. For instance if you don't want a centered track area, you could remove all the lines from `/* Centered track area */` to the next section. Some sections depend on previous ones, like `/* Collapsable settings */` depending on `/* Settings area */` so if you remove the latter, you'll have to remove the former.
